FROM python:3.7-alpine

ADD . /code

WORKDIR /code

RUN pip install -r requirements.txt

# Make default log paths
RUN mkdir -p /var/log/rainfull_crawler

# Add docker-entrypoint
COPY docker-entrypoint.sh /

# Add version info
COPY version.info /

VOLUME /var/log/rainfull_crawler/

CMD ["python", "app.py"]
