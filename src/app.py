import sys
import json
from datetime import datetime
import glob
from urllib import request

def main():
    url = 'https://opendata.cwb.gov.tw/fileapi/v1/opendataapi/O-A0002-001?Authorization=rdec-key-123-45678-011121314&format=JSON'
    req = request.Request(url, {}, method = 'GET')
    resp = request.urlopen(req)
    data = json.loads(resp.read())

    with open('./data/{}.json'.format(datetime.utcnow()), 'w') as f:
        json.dump(data, f)

if __name__ == '__main__':
    sys.exit(main())

